/*
 *   ResultsPage.qml
 *
 *   Copyright (c) 2021 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import org.radar.app 1.0

import "Icon.js" as MdiFont

FocusScope {
    id: root

    clip: true

    readonly property bool currentOSIsAndroid: Qt.platform.os === "android"
    readonly property color evenItemBackground: Material.color(Material.Grey, Material.Shade100)
    readonly property color oddItemBackground: Material.color(Material.Grey, Material.Shade50)
    readonly property color highlightedBackground: Material.color(Material.LightBlue, Material.Shade800)
    readonly property color textColorNormal:  Material.color(Material.Grey, Material.Shade900)
    readonly property color textColorHighlighted:  Material.color(Material.Grey, Material.Shade50)
    readonly property string earliestDate: App.eventsModel.earliestDate

    signal itemClicked(int index);

    function positionToTodaysEvents() {
        console.log("positionToTodaysEvents()");
        var indexOfTodaysFirstEvent = App.getFirstTodaysItemIndex();
        if (indexOfTodaysFirstEvent !== -1) {
            resultsList.positionViewAtIndex(indexOfTodaysFirstEvent, ListView.Beginning);
        }
    }

    function standardBGColor(index) {
        return index % 2 === 0 ? root.evenItemBackground : root.oddItemBackground;
    }

    function isHighlighted(index, mouseArea)
    {
        return resultsList.currentIndex === index ||
                (!root.currentOSIsAndroid && mouseArea.containsMouse);
    }

    function textColor(index, mouseArea)
    {
        return root.isHighlighted(index, mouseArea) ? root.textColorHighlighted : root.textColorNormal;
    }

    BusyIndicator {
        id: busyIndicator

        running: App.state === AppStates.Loading ||
                 App.state === AppStates.Filtering ||
                 App.state === AppStates.Extraction

        anchors.bottom: root.bottom
        anchors.left: root.left
        anchors.right: root.right

        z: 2
    }

    Component {
        id: sectionHeading
        Item {
            id: sectionDelegateRoot
            width: sectionDelegateRoot.ListView.view.width
            height: sectionText.font.pixelSize*2.0

            Rectangle {
                visible: section !== root.earliestDate
                x:0
                y:0
                width: parent.width
                height: 0.5
                color: Material.color(Material.Grey, Material.Shade500)
            }

            Text {
                id: sectionText
                anchors.centerIn: parent
                text: section
                font.capitalization: Font.AllUppercase
                font.pointSize: App.fontPointSize * 1.2
                color: Material.color(Material.Pink, Material.Shade800)
            }
        }
    }

    Label {
        id: noResults

        visible: App.noEventsFound
        anchors.fill: parent

        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        font.pointSize: App.fontPointSize

        text: qsTr("Nothing found in %1").arg(App.city.id === "" ? App.country : App.cityDisplayName(App.city.id))
    }

    ListView {
        id: resultsList

        activeFocusOnTab: true

        visible: !App.noEventsFound

        onVisibleChanged: {
            console.log("!! resultsList.visible=%1".arg(resultsList.visible));
        }

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: busyIndicator.running ? busyIndicator.top : parent.bottom
        Behavior on anchors.bottom {
            PropertyAnimation { duration: 1000 }
        }

        model: App.eventsModel
        highlightFollowsCurrentItem: false
        interactive: true
        keyNavigationEnabled: true

        clip: false

        boundsMovement: Flickable.FollowBoundsBehavior
        boundsBehavior: Flickable.DragOverBounds

        opacity: Math.max(0.0, 1.0 - Math.abs(verticalOvershoot) / height)

        delegate: Loader {
            id: delegateLoader
            width: ListView.view.width
            sourceComponent: MouseArea {
                id: mouseArea

                height: title.contentHeight +
                        2 * title.anchors.margins +
                        Math.max((location.visible ? (location.contentHeight + location.anchors.margins) : 0),
                                 (distance.visible ? (distance.contentHeight + distance.anchors.margins) : 0))

                hoverEnabled: true
                onClicked: {
                    console.log("index: " + index);
                    resultsList.currentIndex = index;
                    mouseArea.forceActiveFocus();
                    root.itemClicked(index);
                }

                Rectangle {
                    anchors.fill: parent
                    color: root.isHighlighted(index, mouseArea) ? root.highlightedBackground : root.standardBGColor(index)
                }

                LayoutMirroring.enabled: true
                LayoutMirroring.childrenInherit: true

                Text {
                    id: date

                    anchors.left: mouseArea.left
                    anchors.top: mouseArea.top
                    anchors.topMargin: 4
                    anchors.leftMargin: 4

                    text: model.startDateTime
                    textFormat: Text.PlainText
                    font.italic: true
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                    color: root.textColor(index, mouseArea)
                    font.pointSize: App.fontPointSize
                }
                Text {
                    id: title

                    font.pixelSize: date.font.pixelSize
                    anchors.top: mouseArea.top
                    anchors.right: mouseArea.right
                    anchors.margins: 4
                    anchors.left: date.right

                    text: {
                        return model.category === "" ? model.title :
                                                       "<strong>%1</strong> [%2]".arg(model.title).arg(model.category);
                    }
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignRight
                    color: root.textColor(index, mouseArea)
                }
                Text {
                    id: distance
                    font.pointSize: date.font.pointSize
                    text: model.distance
                    visible: text !== ""
                    textFormat: Text.PlainText
                    anchors.margins: 4
                    anchors.left: mouseArea.left
                    anchors.bottom: mouseArea.bottom
                    verticalAlignment: Text.AlignBottom
                    color: root.textColor(index, mouseArea)
                }
                Text {
                    id: distanceIcon
                    anchors.left: distance.right
                    anchors.bottom: mouseArea.bottom
                    anchors.margins: 4
                    verticalAlignment: Text.AlignBottom

                    text: MdiFont.Icon.mapMarkerDistance
                    font.family: "Material Design Icons"
                    font.pixelSize: distance.font.pixelSize*1.5

                    visible: distance.visible
                }

                Text {
                    id: location

                    font.pointSize: date.font.pointSize

                    visible: location.text !== ""

                    anchors.margins: 4
                    anchors.bottom: mouseArea.bottom
                    anchors.right: mouseArea.right
                    anchors.left: distance.visible ? distanceIcon.right : date.right

                    text: model.locationName
                    textFormat: Text.PlainText
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignRight
                    color: root.textColor(index, mouseArea)
                }
            }
        }

        section.property: "date"
        section.criteria: ViewSection.FullString
        section.delegate: sectionHeading

        ScrollIndicator.vertical: ScrollIndicator {}
    }

    Connections {
        target: App
        function onFontPointSizeChanged() {
            resultsList.forceLayout();
        }
    }
    Connections {
        target: resultsList
        onVerticalOvershootChanged: {
            console.log("% verticalOvershoot=" + (resultsList.verticalOvershoot/height) * 100);
            if (resultsList.verticalOvershoot > root.height/5) {
                console.log("Load more needed...");
                App.loadMoreEvents();
            }
        }
    }
}
