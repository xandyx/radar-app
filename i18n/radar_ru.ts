<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>App</name>
    <message>
        <location filename="../src/app.cpp" line="841"/>
        <source>Radar App</source>
        <translation>Radar</translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="841"/>
        <source>Link to Android APK: %1</source>
        <translation>Ссылка на Андроид пакет: %1</translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="881"/>
        <source>-- All ---</source>
        <translation>-- все --</translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="949"/>
        <source>%1 days, %2 hours %3 minutes</source>
        <translation>%1 дней, %2 часов %3 минут</translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="951"/>
        <source>%1:%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="983"/>
        <source>%1, %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="1064"/>
        <source>%1
Date: %2
%3

Link: %4</source>
        <translation>%1
Дата: %2
%3

Ссылка: %4</translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="1117"/>
        <source>Event</source>
        <translation>Мероприятие</translation>
    </message>
</context>
<context>
    <name>EventView</name>
    <message>
        <location filename="../ui/EventView.qml" line="139"/>
        <source>Category:</source>
        <translation>Категория:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="161"/>
        <source>at:</source>
        <translation>место:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="212"/>
        <source>Force Plaintext</source>
        <translation>Без форматирования</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="221"/>
        <source>When:</source>
        <translation>Когда:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="242"/>
        <source>Duration:</source>
        <translation>Длительность:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="260"/>
        <source>Price:</source>
        <translation>Цена:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="278"/>
        <source>Address:</source>
        <translation>Адрес:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="327"/>
        <source>View on Map</source>
        <translation>Показать на карте</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="338"/>
        <source>Directions:</source>
        <translation>Проезд:</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="383"/>
        <source>Add to calendar</source>
        <translation>В календарь</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="387"/>
        <source>Plan</source>
        <translation>Запланировать</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="409"/>
        <source>Show in Browser</source>
        <translation>Открыть в браузере</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="413"/>
        <source>View</source>
        <translation>Смотреть</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="436"/>
        <source>Share...</source>
        <translation>Поделиться...</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="440"/>
        <source>Share</source>
        <translation>Поделиться</translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="465"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>LocationPage</name>
    <message>
        <location filename="../ui/LocationPage.qml" line="60"/>
        <source>Country</source>
        <translation>Страна</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="94"/>
        <source>City</source>
        <translation>Город</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="138"/>
        <source>Remember location</source>
        <translation>Запомнить место</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="153"/>
        <source>%1 events in this area</source>
        <translation>Мероприятий в регионе: %1</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="160"/>
        <source>%1 events today</source>
        <translation>Мероприятий сегодня: %1</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="184"/>
        <source>Show &gt;</source>
        <translation>Показать &gt;</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="185"/>
        <source>Show...</source>
        <translation>Показать...</translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="170"/>
        <source>Reload...</source>
        <translation>Перезагрузить...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/eventsmodel.cpp" line="126"/>
        <source>%1 m</source>
        <translation>%1 м</translation>
    </message>
    <message>
        <location filename="../src/eventsmodel.cpp" line="128"/>
        <source>%1 km</source>
        <translation>%1 км</translation>
    </message>
</context>
<context>
    <name>ResultsPage</name>
    <message>
        <location filename="../ui/ResultsPage.qml" line="103"/>
        <source>Nothing found in %1</source>
        <translation>%1: ничего не найдено</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ui/main.qml" line="43"/>
        <location filename="../ui/main.qml" line="294"/>
        <source>Radar App</source>
        <translation>Радар</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="92"/>
        <source>Event on Map</source>
        <translation>Событие на карте</translation>
    </message>
    <message>
        <source>Event</source>
        <translation type="vanished">Мероприятие</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="66"/>
        <source>Location</source>
        <translation>Место</translation>
    </message>
    <message>
        <source>Ongoing events</source>
        <translation type="vanished">Мероприятия</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="70"/>
        <source>Radar</source>
        <translation>Радар</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="264"/>
        <source>Reload Countries</source>
        <translation>Обновить страны</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="270"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="275"/>
        <source>Share App...</source>
        <translation>Поделиться приложением...</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="280"/>
        <source>Share via QR-code...</source>
        <translation>Поделиться через QR-код...</translation>
    </message>
    <message>
        <source>&lt;p&gt;Copyright © 2019-2020&lt;/p&gt;&lt;p&gt;This program comes with ABSOLUTELY NO WARRANTY.&lt;/p&gt;&lt;p&gt;This is free software, and you are welcome to redistribute it under certain conditions.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;Details on License…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Source code…&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Copyright © 2019-2021&lt;/p&gt;&lt;p&gt;Программа поставляется БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ.&lt;/p&gt;&lt;p&gt;Это Свободное Программное Обеспечение, распространение на определённых условиях приветствуется.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.ru.html&quot;&gt;Подробнее о лицензии…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Исходный код…&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Copyright © 2021&lt;/p&gt;&lt;p&gt;This program comes with ABSOLUTELY NO WARRANTY.&lt;/p&gt;&lt;p&gt;This is free software, and you are welcome to redistribute it under certain conditions.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;Details on License…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Source code…&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Copyright © 2019-2021&lt;/p&gt;&lt;p&gt;Программа поставляется БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ.&lt;/p&gt;&lt;p&gt;Это Свободное Программное Обеспечение, распространение на определённых условиях приветствуется.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.ru.html&quot;&gt;Подробнее о лицензии…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Исходный код…&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="307"/>
        <source>&lt;p&gt;Copyright © 2023&lt;/p&gt;&lt;p&gt;This program comes with ABSOLUTELY NO WARRANTY.&lt;/p&gt;&lt;p&gt;This is free software, and you are welcome to redistribute it under certain conditions.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;Details on License…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Source code…&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Copyright © 2019-2021&lt;/p&gt;&lt;p&gt;Программа поставляется БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ.&lt;/p&gt;&lt;p&gt;Это Свободное Программное Обеспечение, распространение на определённых условиях приветствуется.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.ru.html&quot;&gt;Подробнее о лицензии…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Исходный код…&lt;/a&gt;&lt;/p&gt; {2023&lt;?} {3.0.?} {0x?}</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="328"/>
        <source>Scan QR Code to download</source>
        <translation>Отсканируйте QR-код для скачивания</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="363"/>
        <source>No Maps application available.</source>
        <translation>Нет приложения Карты.</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="367"/>
        <source>Do you want to see event location with web browser?</source>
        <translation>Желаете ли вы открыть местоположение мероприятия в браузере?</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="383"/>
        <source>Failed to load data</source>
        <translation>Сбой загрузки данных</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="384"/>
        <source>Network Errror</source>
        <translation>Ошибка сети</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="469"/>
        <source>&lt; Back</source>
        <translation>&lt; Назад</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="494"/>
        <source>Next &gt;</source>
        <translation>Далее &gt;</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="590"/>
        <source>Please wait…</source>
        <translation>Подождите...</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="620"/>
        <source>Getting events</source>
        <translation>Загрузка мероприятий</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="623"/>
        <source>Filtering events</source>
        <translation>Фильтрация мероприятий</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="626"/>
        <source>Loading countries</source>
        <translation>Загрузка стран</translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="629"/>
        <source>Loading cities</source>
        <translation>Загрузка городов</translation>
    </message>
</context>
<context>
    <name>map</name>
    <message>
        <location filename="../ui/map.qml" line="66"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
</TS>
