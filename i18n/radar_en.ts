<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>App</name>
    <message>
        <location filename="../src/app.cpp" line="841"/>
        <source>Radar App</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="841"/>
        <source>Link to Android APK: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="881"/>
        <source>-- All ---</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="949"/>
        <source>%1 days, %2 hours %3 minutes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="951"/>
        <source>%1:%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="983"/>
        <source>%1, %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="1064"/>
        <source>%1
Date: %2
%3

Link: %4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/app.cpp" line="1117"/>
        <source>Event</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EventView</name>
    <message>
        <location filename="../ui/EventView.qml" line="139"/>
        <source>Category:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="161"/>
        <source>at:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="212"/>
        <source>Force Plaintext</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="221"/>
        <source>When:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="242"/>
        <source>Duration:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="260"/>
        <source>Price:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="278"/>
        <source>Address:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="327"/>
        <source>View on Map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="338"/>
        <source>Directions:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="383"/>
        <source>Add to calendar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="387"/>
        <source>Plan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="409"/>
        <source>Show in Browser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="413"/>
        <source>View</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="436"/>
        <source>Share...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="440"/>
        <source>Share</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/EventView.qml" line="465"/>
        <source>Close</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LocationPage</name>
    <message>
        <location filename="../ui/LocationPage.qml" line="60"/>
        <source>Country</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="94"/>
        <source>City</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="138"/>
        <source>Remember location</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="153"/>
        <source>%1 events in this area</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="160"/>
        <source>%1 events today</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="170"/>
        <source>Reload...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="184"/>
        <source>Show &gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/LocationPage.qml" line="185"/>
        <source>Show...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/eventsmodel.cpp" line="126"/>
        <source>%1 m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/eventsmodel.cpp" line="128"/>
        <source>%1 km</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ResultsPage</name>
    <message>
        <location filename="../ui/ResultsPage.qml" line="103"/>
        <source>Nothing found in %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ui/main.qml" line="43"/>
        <location filename="../ui/main.qml" line="294"/>
        <source>Radar App</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="92"/>
        <source>Event on Map</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="66"/>
        <source>Location</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="70"/>
        <source>Radar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="264"/>
        <source>Reload Countries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="270"/>
        <source>About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="275"/>
        <source>Share App...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="280"/>
        <source>Share via QR-code...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="307"/>
        <source>&lt;p&gt;Copyright © 2023&lt;/p&gt;&lt;p&gt;This program comes with ABSOLUTELY NO WARRANTY.&lt;/p&gt;&lt;p&gt;This is free software, and you are welcome to redistribute it under certain conditions.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;Details on License…&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://0xacab.org/xandyx/radar-app/tree/master&quot;&gt;Source code…&lt;/a&gt;&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="328"/>
        <source>Scan QR Code to download</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="363"/>
        <source>No Maps application available.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="367"/>
        <source>Do you want to see event location with web browser?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="383"/>
        <source>Failed to load data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="384"/>
        <source>Network Errror</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="469"/>
        <source>&lt; Back</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="494"/>
        <source>Next &gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="590"/>
        <source>Please wait…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="620"/>
        <source>Getting events</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="623"/>
        <source>Filtering events</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="626"/>
        <source>Loading countries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/main.qml" line="629"/>
        <source>Loading cities</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>map</name>
    <message>
        <location filename="../ui/map.qml" line="66"/>
        <source>Close</source>
        <translation></translation>
    </message>
</context>
</TS>
