/*
 *   all_countries.cpp
 *
 *   Copyright (c) 2021 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "all_countries.h"

#include <QLocale>

#include <QMetaEnum>

QMap< QString, QByteArray > Countries::allCountries()
{
    static QMap< QString, QByteArray > countries{
        {QStringLiteral("Afghanistan"), QByteArrayLiteral("AF")},
        {QStringLiteral("Åland Islands"), QByteArrayLiteral("AX")},
        {QStringLiteral("Albania"), QByteArrayLiteral("AL")},
        {QStringLiteral("Algeria"), QByteArrayLiteral("DZ")},
        {QStringLiteral("American Samoa"), QByteArrayLiteral("AS")},
        {QStringLiteral("Andorra"), QByteArrayLiteral("AD")},
        {QStringLiteral("Angola"), QByteArrayLiteral("AO")},
        {QStringLiteral("Anguilla"), QByteArrayLiteral("AI")},
        {QStringLiteral("Antarctica"), QByteArrayLiteral("AQ")},
        {QStringLiteral("Antigua and Barbuda"), QByteArrayLiteral("AG")},
        {QStringLiteral("Argentina"), QByteArrayLiteral("AR")},
        {QStringLiteral("Armenia"), QByteArrayLiteral("AM")},
        {QStringLiteral("Aruba"), QByteArrayLiteral("AW")},
        {QStringLiteral("Australia"), QByteArrayLiteral("AU")},
        {QStringLiteral("Austria"), QByteArrayLiteral("AT")},
        {QStringLiteral("Azerbaijan"), QByteArrayLiteral("AZ")},
        {QStringLiteral("Bahamas"), QByteArrayLiteral("BS")},
        {QStringLiteral("Bahrain"), QByteArrayLiteral("BH")},
        {QStringLiteral("Bangladesh"), QByteArrayLiteral("BD")},
        {QStringLiteral("Basque"), QByteArrayLiteral("XE")},
        {QStringLiteral("Barbados"), QByteArrayLiteral("BB")},
        {QStringLiteral("Belarus"), QByteArrayLiteral("BY")},
        {QStringLiteral("Belgium"), QByteArrayLiteral("BE")},
        {QStringLiteral("Belize"), QByteArrayLiteral("BZ")},
        {QStringLiteral("Benin"), QByteArrayLiteral("BJ")},
        {QStringLiteral("Bermuda"), QByteArrayLiteral("BM")},
        {QStringLiteral("Bhutan"), QByteArrayLiteral("BT")},
        {QStringLiteral("Bolivia"), QByteArrayLiteral("BO")},
        {QStringLiteral("Bonaire, Sint Eustatius and Saba"), QByteArrayLiteral("BQ")},
        {QStringLiteral("Bosnia and Herzegovina"), QByteArrayLiteral("BA")},
        {QStringLiteral("Botswana"), QByteArrayLiteral("BW")},
        {QStringLiteral("Bouvet Island"), QByteArrayLiteral("BV")},
        {QStringLiteral("Brazil"), QByteArrayLiteral("BR")},
        {QStringLiteral("British Indian Ocean Territory"), QByteArrayLiteral("IO")},
        {QStringLiteral("Brunei Darussalam"), QByteArrayLiteral("BN")},
        {QStringLiteral("Bulgaria"), QByteArrayLiteral("BG")},
        {QStringLiteral("Burkina Faso"), QByteArrayLiteral("BF")},
        {QStringLiteral("Burundi"), QByteArrayLiteral("BI")},
        {QStringLiteral("Cabo Verde"), QByteArrayLiteral("CV")},
        {QStringLiteral("Cambodia"), QByteArrayLiteral("KH")},
        {QStringLiteral("Cameroon"), QByteArrayLiteral("CM")},
        {QStringLiteral("Canada"), QByteArrayLiteral("CA")},
        {QStringLiteral("Catalonia"), QByteArrayLiteral("XC")},
        {QStringLiteral("Cayman Islands"), QByteArrayLiteral("KY")},
        {QStringLiteral("Central African Republic"), QByteArrayLiteral("CF")},
        {QStringLiteral("Chad"), QByteArrayLiteral("TD")},
        {QStringLiteral("Chile"), QByteArrayLiteral("CL")},
        {QStringLiteral("China"), QByteArrayLiteral("CN")},
        {QStringLiteral("Christmas Island"), QByteArrayLiteral("CX")},
        {QStringLiteral("Cocos (Keeling) Islands"), QByteArrayLiteral("CC")},
        {QStringLiteral("Colombia"), QByteArrayLiteral("CO")},
        {QStringLiteral("Comoros"), QByteArrayLiteral("KM")},
        {QStringLiteral("Congo"), QByteArrayLiteral("CG")},
        {QStringLiteral("Congo, Democratic Republic of the"), QByteArrayLiteral("CD")},
        {QStringLiteral("Cook Islands"), QByteArrayLiteral("CK")},
        {QStringLiteral("Costa Rica"), QByteArrayLiteral("CR")},
        {QStringLiteral("Côte d'Ivoire"), QByteArrayLiteral("CI")},
        {QStringLiteral("Croatia"), QByteArrayLiteral("HR")},
        {QStringLiteral("Cuba"), QByteArrayLiteral("CU")},
        {QStringLiteral("Curaçao"), QByteArrayLiteral("CW")},
        {QStringLiteral("Cyprus"), QByteArrayLiteral("CY")},
        {QStringLiteral("Czechia"), QByteArrayLiteral("CZ")},
        {QStringLiteral("Denmark"), QByteArrayLiteral("DK")},
        {QStringLiteral("Djibouti"), QByteArrayLiteral("DJ")},
        {QStringLiteral("Dominica"), QByteArrayLiteral("DM")},
        {QStringLiteral("Dominican Republic"), QByteArrayLiteral("DO")},
        {QStringLiteral("Ecuador"), QByteArrayLiteral("EC")},
        {QStringLiteral("Egypt"), QByteArrayLiteral("EG")},
        {QStringLiteral("El Salvador"), QByteArrayLiteral("SV")},
        {QStringLiteral("Equatorial Guinea"), QByteArrayLiteral("GQ")},
        {QStringLiteral("Eritrea"), QByteArrayLiteral("ER")},
        {QStringLiteral("Estonia"), QByteArrayLiteral("EE")},
        {QStringLiteral("Eswatini"), QByteArrayLiteral("SZ")},
        {QStringLiteral("Ethiopia"), QByteArrayLiteral("ET")},
        {QStringLiteral("Falkland Islands (Malvinas)"), QByteArrayLiteral("FK")},
        {QStringLiteral("Faroe Islands"), QByteArrayLiteral("FO")},
        {QStringLiteral("Fiji"), QByteArrayLiteral("FJ")},
        {QStringLiteral("Finland"), QByteArrayLiteral("FI")},
        {QStringLiteral("France"), QByteArrayLiteral("FR")},
        {QStringLiteral("French Guiana"), QByteArrayLiteral("GF")},
        {QStringLiteral("French Polynesia"), QByteArrayLiteral("PF")},
        {QStringLiteral("French Southern Territories"), QByteArrayLiteral("TF")},
        {QStringLiteral("Gabon"), QByteArrayLiteral("GA")},
        {QStringLiteral("Gambia"), QByteArrayLiteral("GM")},
        {QStringLiteral("Georgia"), QByteArrayLiteral("GE")},
        {QStringLiteral("Germany"), QByteArrayLiteral("DE")},
        {QStringLiteral("Ghana"), QByteArrayLiteral("GH")},
        {QStringLiteral("Gibraltar"), QByteArrayLiteral("GI")},
        {QStringLiteral("Greece"), QByteArrayLiteral("GR")},
        {QStringLiteral("Greenland"), QByteArrayLiteral("GL")},
        {QStringLiteral("Grenada"), QByteArrayLiteral("GD")},
        {QStringLiteral("Guadeloupe"), QByteArrayLiteral("GP")},
        {QStringLiteral("Guam"), QByteArrayLiteral("GU")},
        {QStringLiteral("Guatemala"), QByteArrayLiteral("GT")},
        {QStringLiteral("Guernsey"), QByteArrayLiteral("GG")},
        {QStringLiteral("Guinea"), QByteArrayLiteral("GN")},
        {QStringLiteral("Guinea-Bissau"), QByteArrayLiteral("GW")},
        {QStringLiteral("Guyana"), QByteArrayLiteral("GY")},
        {QStringLiteral("Haiti"), QByteArrayLiteral("HT")},
        {QStringLiteral("Heard Island and McDonald Islands"), QByteArrayLiteral("HM")},
        {QStringLiteral("Holy See"), QByteArrayLiteral("VA")},
        {QStringLiteral("Honduras"), QByteArrayLiteral("HN")},
        {QStringLiteral("Hong Kong"), QByteArrayLiteral("HK")},
        {QStringLiteral("Hungary"), QByteArrayLiteral("HU")},
        {QStringLiteral("Iceland"), QByteArrayLiteral("IS")},
        {QStringLiteral("India"), QByteArrayLiteral("IN")},
        {QStringLiteral("Indonesia"), QByteArrayLiteral("ID")},
        {QStringLiteral("Iran (Islamic Republic of)"), QByteArrayLiteral("IR")},
        {QStringLiteral("Iraq"), QByteArrayLiteral("IQ")},
        {QStringLiteral("Ireland"), QByteArrayLiteral("IE")},
        {QStringLiteral("Isle of Man"), QByteArrayLiteral("IM")},
        {QStringLiteral("Israel"), QByteArrayLiteral("IL")},
        {QStringLiteral("Italy"), QByteArrayLiteral("IT")},
        {QStringLiteral("Jamaica"), QByteArrayLiteral("JM")},
        {QStringLiteral("Japan"), QByteArrayLiteral("JP")},
        {QStringLiteral("Jersey"), QByteArrayLiteral("JE")},
        {QStringLiteral("Jordan"), QByteArrayLiteral("JO")},
        {QStringLiteral("Kazakhstan"), QByteArrayLiteral("KZ")},
        {QStringLiteral("Kenya"), QByteArrayLiteral("KE")},
        {QStringLiteral("Kiribati"), QByteArrayLiteral("KI")},
        {QStringLiteral("Korea (North)"), QByteArrayLiteral("KP")},
        {QStringLiteral("Korea (South)"), QByteArrayLiteral("KR")},
        {QStringLiteral("Kosovo"), QByteArrayLiteral("XK")},
        {QStringLiteral("Kuwait"), QByteArrayLiteral("KW")},
        {QStringLiteral("Kyrgyzstan"), QByteArrayLiteral("KG")},
        {QStringLiteral("Lao People's Democratic Republic"), QByteArrayLiteral("LA")},
        {QStringLiteral("Latvia"), QByteArrayLiteral("LV")},
        {QStringLiteral("Lebanon"), QByteArrayLiteral("LB")},
        {QStringLiteral("Lesotho"), QByteArrayLiteral("LS")},
        {QStringLiteral("Liberia"), QByteArrayLiteral("LR")},
        {QStringLiteral("Libya"), QByteArrayLiteral("LY")},
        {QStringLiteral("Liechtenstein"), QByteArrayLiteral("LI")},
        {QStringLiteral("Lithuania"), QByteArrayLiteral("LT")},
        {QStringLiteral("Luxembourg"), QByteArrayLiteral("LU")},
        {QStringLiteral("Macao"), QByteArrayLiteral("MO")},
        {QStringLiteral("Madagascar"), QByteArrayLiteral("MG")},
        {QStringLiteral("Malawi"), QByteArrayLiteral("MW")},
        {QStringLiteral("Malaysia"), QByteArrayLiteral("MY")},
        {QStringLiteral("Maldives"), QByteArrayLiteral("MV")},
        {QStringLiteral("Mali"), QByteArrayLiteral("ML")},
        {QStringLiteral("Malta"), QByteArrayLiteral("MT")},
        {QStringLiteral("Marshall Islands"), QByteArrayLiteral("MH")},
        {QStringLiteral("Martinique"), QByteArrayLiteral("MQ")},
        {QStringLiteral("Mauritania"), QByteArrayLiteral("MR")},
        {QStringLiteral("Mauritius"), QByteArrayLiteral("MU")},
        {QStringLiteral("Mayotte"), QByteArrayLiteral("YT")},
        {QStringLiteral("Mexico"), QByteArrayLiteral("MX")},
        {QStringLiteral("Micronesia"), QByteArrayLiteral("FM")},
        {QStringLiteral("Moldova"), QByteArrayLiteral("MD")},
        {QStringLiteral("Monaco"), QByteArrayLiteral("MC")},
        {QStringLiteral("Mongolia"), QByteArrayLiteral("MN")},
        {QStringLiteral("Montenegro"), QByteArrayLiteral("ME")},
        {QStringLiteral("Montserrat"), QByteArrayLiteral("MS")},
        {QStringLiteral("Morocco"), QByteArrayLiteral("MA")},
        {QStringLiteral("Mozambique"), QByteArrayLiteral("MZ")},
        {QStringLiteral("Myanmar"), QByteArrayLiteral("MM")},
        {QStringLiteral("Namibia"), QByteArrayLiteral("NA")},
        {QStringLiteral("Nauru"), QByteArrayLiteral("NR")},
        {QStringLiteral("Nepal"), QByteArrayLiteral("NP")},
        {QStringLiteral("Netherlands"), QByteArrayLiteral("NL")},
        {QStringLiteral("New Caledonia"), QByteArrayLiteral("NC")},
        {QStringLiteral("New Zealand"), QByteArrayLiteral("NZ")},
        {QStringLiteral("Nicaragua"), QByteArrayLiteral("NI")},
        {QStringLiteral("Niger"), QByteArrayLiteral("NE")},
        {QStringLiteral("Nigeria"), QByteArrayLiteral("NG")},
        {QStringLiteral("Niue"), QByteArrayLiteral("NU")},
        {QStringLiteral("Norfolk Island"), QByteArrayLiteral("NF")},
        {QStringLiteral("North Macedonia"), QByteArrayLiteral("MK")},
        {QStringLiteral("Northern Mariana Islands"), QByteArrayLiteral("MP")},
        {QStringLiteral("Norway"), QByteArrayLiteral("NO")},
        {QStringLiteral("Oman"), QByteArrayLiteral("OM")},
        {QStringLiteral("Pakistan"), QByteArrayLiteral("PK")},
        {QStringLiteral("Palau"), QByteArrayLiteral("PW")},
        {QStringLiteral("Palestine, State of"), QByteArrayLiteral("PS")},
        {QStringLiteral("Panama"), QByteArrayLiteral("PA")},
        {QStringLiteral("Papua New Guinea"), QByteArrayLiteral("PG")},
        {QStringLiteral("Paraguay"), QByteArrayLiteral("PY")},
        {QStringLiteral("Peru"), QByteArrayLiteral("PE")},
        {QStringLiteral("Philippines"), QByteArrayLiteral("PH")},
        {QStringLiteral("Pitcairn"), QByteArrayLiteral("PN")},
        {QStringLiteral("Poland"), QByteArrayLiteral("PL")},
        {QStringLiteral("Portugal"), QByteArrayLiteral("PT")},
        {QStringLiteral("Puerto Rico"), QByteArrayLiteral("PR")},
        {QStringLiteral("Qatar"), QByteArrayLiteral("QA")},
        {QStringLiteral("Réunion"), QByteArrayLiteral("RE")},
        {QStringLiteral("Romania"), QByteArrayLiteral("RO")},
        {QStringLiteral("Russia"), QByteArrayLiteral("RU")},
        {QStringLiteral("Rwanda"), QByteArrayLiteral("RW")},
        {QStringLiteral("Saint Barthélemy"), QByteArrayLiteral("BL")},
        {QStringLiteral("Saint Helena, Ascension and Tristan da Cunha"), QByteArrayLiteral("SH")},
        {QStringLiteral("Saint Kitts and Nevis"), QByteArrayLiteral("KN")},
        {QStringLiteral("Saint Lucia"), QByteArrayLiteral("LC")},
        {QStringLiteral("Saint Martin (French part)"), QByteArrayLiteral("MF")},
        {QStringLiteral("Saint Pierre and Miquelon"), QByteArrayLiteral("PM")},
        {QStringLiteral("Saint Vincent and the Grenadines"), QByteArrayLiteral("VC")},
        {QStringLiteral("Samoa"), QByteArrayLiteral("WS")},
        {QStringLiteral("San Marino"), QByteArrayLiteral("SM")},
        {QStringLiteral("Sao Tome and Principe"), QByteArrayLiteral("ST")},
        {QStringLiteral("Saudi Arabia"), QByteArrayLiteral("SA")},
        {QStringLiteral("Senegal"), QByteArrayLiteral("SN")},
        {QStringLiteral("Serbia"), QByteArrayLiteral("RS")},
        {QStringLiteral("Seychelles"), QByteArrayLiteral("SC")},
        {QStringLiteral("Sierra Leone"), QByteArrayLiteral("SL")},
        {QStringLiteral("Singapore"), QByteArrayLiteral("SG")},
        {QStringLiteral("Sint Maarten (Dutch part)"), QByteArrayLiteral("SX")},
        {QStringLiteral("Slovakia"), QByteArrayLiteral("SK")},
        {QStringLiteral("Slovenia"), QByteArrayLiteral("SI")},
        {QStringLiteral("Solomon Islands"), QByteArrayLiteral("SB")},
        {QStringLiteral("Somalia"), QByteArrayLiteral("SO")},
        {QStringLiteral("South Africa"), QByteArrayLiteral("ZA")},
        {QStringLiteral("South Georgia and the South Sandwich Islands"), QByteArrayLiteral("GS")},
        {QStringLiteral("South Sudan"), QByteArrayLiteral("SS")},
        {QStringLiteral("Spain"), QByteArrayLiteral("ES")},
        {QStringLiteral("Sri Lanka"), QByteArrayLiteral("LK")},
        {QStringLiteral("Sudan"), QByteArrayLiteral("SD")},
        {QStringLiteral("Suriname"), QByteArrayLiteral("SR")},
        {QStringLiteral("Svalbard and Jan Mayen"), QByteArrayLiteral("SJ")},
        {QStringLiteral("Sweden"), QByteArrayLiteral("SE")},
        {QStringLiteral("Switzerland"), QByteArrayLiteral("CH")},
        {QStringLiteral("Syrian Arab Republic"), QByteArrayLiteral("SY")},
        {QStringLiteral("Taiwan, Province of China"), QByteArrayLiteral("TW")},
        {QStringLiteral("Tajikistan"), QByteArrayLiteral("TJ")},
        {QStringLiteral("Tanzania, United Republic of"), QByteArrayLiteral("TZ")},
        {QStringLiteral("Thailand"), QByteArrayLiteral("TH")},
        {QStringLiteral("Timor-Leste"), QByteArrayLiteral("TL")},
        {QStringLiteral("Togo"), QByteArrayLiteral("TG")},
        {QStringLiteral("Tokelau"), QByteArrayLiteral("TK")},
        {QStringLiteral("Tonga"), QByteArrayLiteral("TO")},
        {QStringLiteral("Trinidad and Tobago"), QByteArrayLiteral("TT")},
        {QStringLiteral("Tunisia"), QByteArrayLiteral("TN")},
        {QStringLiteral("Turkey"), QByteArrayLiteral("TR")},
        {QStringLiteral("Turkmenistan"), QByteArrayLiteral("TM")},
        {QStringLiteral("Turks and Caicos Islands"), QByteArrayLiteral("TC")},
        {QStringLiteral("Tuvalu"), QByteArrayLiteral("TV")},
        {QStringLiteral("Uganda"), QByteArrayLiteral("UG")},
        {QStringLiteral("Ukraine"), QByteArrayLiteral("UA")},
        {QStringLiteral("United Arab Emirates"), QByteArrayLiteral("AE")},
        {QStringLiteral("United Kingdom"), QByteArrayLiteral("GB")},
        {QStringLiteral("United States of America"), QByteArrayLiteral("US")},
        {QStringLiteral("United States Minor Outlying Islands"), QByteArrayLiteral("UM")},
        {QStringLiteral("Uruguay"), QByteArrayLiteral("UY")},
        {QStringLiteral("Uzbekistan"), QByteArrayLiteral("UZ")},
        {QStringLiteral("Vanuatu"), QByteArrayLiteral("VU")},
        {QStringLiteral("Venezuela"), QByteArrayLiteral("VE")},
        {QStringLiteral("Viet Nam"), QByteArrayLiteral("VN")},
        {QStringLiteral("Virgin Islands (British)"), QByteArrayLiteral("VG")},
        {QStringLiteral("Virgin Islands (U.S.)"), QByteArrayLiteral("VI")},
        {QStringLiteral("Wallis and Futuna"), QByteArrayLiteral("WF")},
        {QStringLiteral("Western Sahara"), QByteArrayLiteral("EH")},
        {QStringLiteral("Yemen"), QByteArrayLiteral("YE")},
        {QStringLiteral("Zambia"), QByteArrayLiteral("ZM")},
        {QStringLiteral("Zimbabwe"), QByteArrayLiteral("ZW")}};
    return countries;
}

QByteArray Countries::countryCode(const QString &country)
{
    return allCountries().value(country, QByteArray());
}

namespace
{
    QHash< QString, QLocale::Country > countryByCodeMap()
    {
        const auto metaEnum = QMetaEnum::fromType< QLocale::Country >();
        QHash< QString, QLocale::Country > result;
        result.reserve(metaEnum.keyCount());
        for (int i = 0, count = metaEnum.keyCount(); i < count; i++) {
            QLocale locale(QLocale::Language::AnyLanguage, static_cast< QLocale::Country >(metaEnum.value(i)));
            const auto code = locale.name().split(QChar('_')).last();
            if (code.isEmpty()) {
                continue;
            }
            result.insert(code, locale.country());
        }
        return result;
    }
}// namespace

QString Countries::countryByCode(const QByteArray &code)
{
    static auto mapping = countryByCodeMap();
    auto foundIt = mapping.constFind(code);
    if (mapping.cend() != foundIt) {
        return QLocale::countryToString(foundIt.value());
    }
    return allCountries().key(code, code);
}
