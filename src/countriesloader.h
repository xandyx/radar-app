/*
 *   countriesloader.h
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QObject>

class CountriesLoader : public QObject
{
    Q_OBJECT
public:
    explicit CountriesLoader(QNetworkAccessManager &networkAccessManager, QObject *parent = nullptr);
    ~CountriesLoader() override;

    void loadCountries();
    const QJsonObject &locations() const;
    void clear();

signals:
    void loadCompleted(QPrivateSignal);
    void loadFailed(QPrivateSignal);

private:
    QNetworkAccessManager *const m_networkAccessManager;
    const QString m_requestUrlBase;

    QJsonObject m_locations;
};
