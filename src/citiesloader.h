/*
 *   countriesloader.h
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QByteArray>
#include <QJsonObject>
#include <QMap>
#include <QNetworkAccessManager>
#include <QObject>
#include <QSet>

class DB;

class CitiesLoader : public QObject
{
    Q_OBJECT
public:
    explicit CitiesLoader(QNetworkAccessManager &networkAccessManager, DB &db, QObject *parent = nullptr);
    ~CitiesLoader() override;

    bool checkCountriesToLoad(const QStringList &allCountries);

    void loadCities();
    void clear();

    void loadCitiesFromDB();

    const QMap<QByteArray, QStringList> &citiesByCountryCode() const;

signals:
    void allCitiesLoaded(QPrivateSignal);
    void loadFailed(QPrivateSignal);

private:
    QNetworkAccessManager *const m_networkAccessManager;
    DB *const m_db;

    const QString m_requestUrlBase;

    QStringList m_allCountries;

    QMap< QByteArray, QStringList > m_citiesByCountryCode;
    QSet< QByteArray > m_countriesToLoad;

    QJsonObject m_locations;
};
