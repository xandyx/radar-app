/*
 *   requestutils.cpp
 *
 *   Copyright (c) 2024 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "requestutils.h"

#include <qssl.h>

#include <QSslConfiguration>
#include <QNetworkRequest>

void configureRequestSSL(QNetworkRequest &request)
{
    QSslConfiguration sslConfig{QSslConfiguration::defaultConfiguration()};
    sslConfig.setProtocol(QSsl::AnyProtocol);

    request.setSslConfiguration(sslConfig);
}
