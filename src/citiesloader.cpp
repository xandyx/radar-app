/*
 *   countriesloader.cpp
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "citiesloader.h"

#include "all_countries.h"
#include "database.h"
#include "requestutils.h"

#include <QJsonArray>
#include <QJsonParseError>
#include <QNetworkReply>

CitiesLoader::CitiesLoader(QNetworkAccessManager &networkAccessManager, DB &db, QObject *parent)
    : QObject(parent)
    , m_networkAccessManager(&networkAccessManager)
    , m_db(&db)
    , m_requestUrlBase(
          QStringLiteral("https://radar.squat.net/api/1.2/search/groups.json?fields[]=uuid&limit=1&facets[country][]=%1"))
{
}

bool CitiesLoader::checkCountriesToLoad(const QStringList &allCountries)
{
    m_allCountries = allCountries;
    if (m_allCountries.isEmpty()) {
        emit loadFailed(QPrivateSignal());
        return false;
    }
    return true;
}

CitiesLoader::~CitiesLoader() = default;

void CitiesLoader::loadCities()
{
    if (m_networkAccessManager->networkAccessible() == QNetworkAccessManager::NetworkAccessibility::UnknownAccessibility) {
        m_networkAccessManager->setNetworkAccessible((QNetworkAccessManager::NetworkAccessibility::Accessible));
    }
    if (m_networkAccessManager->networkAccessible() != QNetworkAccessManager::NetworkAccessibility::Accessible) {
        qCritical() << "Network is not accessible! networkAccessManager->networkAccessible()=" << m_networkAccessManager->networkAccessible();
        emit loadFailed(QPrivateSignal());
        return;
    }

    QList< QByteArray > allCodes;
    const auto &countryCodes = Countries::allCountries();
    allCodes.reserve(countryCodes.size());
    for (const auto &country : qAsConst(m_allCountries)) {
        allCodes.push_back(Countries::countryCode(country));
    }

    m_countriesToLoad = QSet< QByteArray >::fromList(allCodes);

    for (const auto &code : qAsConst(allCodes)) {
        const auto countryCode = code.toUpper();
        QUrl requestUrl(m_requestUrlBase.arg(QLatin1String(countryCode)));
        qDebug() << "Request URL:" << requestUrl.toString();
        QNetworkRequest request;
        configureRequestSSL(request);
        request.setUrl(requestUrl);
        request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("Radar App 1.0"));
        auto reply = m_networkAccessManager->get(request);
        connect(reply, &QNetworkReply::finished, this, [ this, reply, countryCode ]() noexcept {
            qDebug() << "reply.error" << reply->error();
            qDebug() << "reply.isFinished = " << reply->isFinished();
            qDebug() << "reply.url" << reply->url();
            qDebug() << "reply.size:" << reply->size();
            qDebug() << "Content-Type" << reply->header(QNetworkRequest::KnownHeaders::ContentTypeHeader);
            if (reply->isFinished()) {
                m_countriesToLoad.remove(countryCode);
                qDebug() << "Remaining countries:" << m_countriesToLoad;
                QByteArray buf = reply->readAll();
                QJsonParseError err{};
                QJsonDocument json = QJsonDocument::fromJson(buf, &err);
                if (json.isNull()) {
                    qCritical() << "Json parse error:" << err.errorString();
                    emit loadFailed(QPrivateSignal());
                }
                const auto &cities = json.object();
                const auto facets = cities.constFind(QLatin1String("facets"));
                if (facets != cities.constEnd()) {
                    const auto &facetsObj = facets->toObject();
                    const auto cityArray = facetsObj.constFind(QLatin1String("city"));
                    if (cityArray != facetsObj.constEnd()) {
                        QStringList citiesForCountry;
                        const auto &jsonArray = cityArray->toArray();
                        citiesForCountry.reserve(jsonArray.size());
                        for (const auto &city : jsonArray) {
                            const auto &cityObj = city.toObject();
                            citiesForCountry.push_back(cityObj.value(QLatin1String("filter")).toString());
                        }
                        qDebug() << "Country: " << countryCode << " Cities:" << citiesForCountry;
                        m_citiesByCountryCode.insert(countryCode, citiesForCountry);
                    }
                }
                reply->close();
                reply->deleteLater();
                if (m_countriesToLoad.empty()) {
                    qDebug() << "cities by country:" << m_citiesByCountryCode;
                    for (auto it = m_citiesByCountryCode.constBegin(), end = m_citiesByCountryCode.constEnd(); it != end;
                         ++it) {
                        m_db->insertCountry(it.key(), Countries::allCountries().key(it.key()), it.value());
                    }
                    emit this->allCitiesLoaded(QPrivateSignal());
                }
            }
        });
    }
}

void CitiesLoader::clear()
{
    m_countriesToLoad.clear();
    m_citiesByCountryCode.clear();
}

void CitiesLoader::loadCitiesFromDB()
{
    m_citiesByCountryCode = m_db->getAllCities();
}

const QMap< QByteArray, QStringList > &CitiesLoader::citiesByCountryCode() const
{
    return m_citiesByCountryCode;
}
