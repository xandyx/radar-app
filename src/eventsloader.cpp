/*
 *   eventsloader.cpp
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "eventsloader.h"
#include "database.h"
#include "requestutils.h"

EventsLoader::EventsLoader(QNetworkAccessManager &networkAccessManager,
                           DB &db,
                           QObject *parent)
    : QObject{parent}
    , m_networkAccessManager(&networkAccessManager)
    , m_db(&db)
    , m_eventsRequestUrlBase(QStringLiteral("https://radar.squat.net/api/1.2/search/events.json"))
{
}

EventsLoader::~EventsLoader() = default;

void EventsLoader::loadEvents(const QUrlQuery &query)
{
    const QVariant &cachedEvents = m_db->getCachedEvents(query.toString());
    if (cachedEvents.isValid() && !cachedEvents.isNull()) {
        QJsonDocument json = QJsonDocument::fromJson(cachedEvents.toByteArray());
        if (json.isNull()) {
            qCritical() << "Json parse error!";
            m_db->clearCachedEvents(query.toString());
        } else {
            m_events = json.object();
            qInfo() << "Cache hit!";
            emit this->loadCompleted(QPrivateSignal());
            return;
        }
    }

    if (m_networkAccessManager->networkAccessible() == QNetworkAccessManager::NetworkAccessibility::UnknownAccessibility) {
        m_networkAccessManager->setNetworkAccessible((QNetworkAccessManager::NetworkAccessibility::Accessible));
    }
    if (m_networkAccessManager->networkAccessible() != QNetworkAccessManager::NetworkAccessibility::Accessible) {
        qCritical() << "Network is not accessible! networkAccessManager->networkAccessible()=" << m_networkAccessManager->networkAccessible();
        emit loadFailed(QPrivateSignal());
        return;
    }
    QUrl requestUrl(m_eventsRequestUrlBase, QUrl::ParsingMode::TolerantMode);
    requestUrl.setQuery(query);
    qDebug() << "requestUrl=" << requestUrl.toString();
    QNetworkRequest request;
    configureRequestSSL(request);
    request.setUrl(requestUrl);
    request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("Radar App 1.0"));

    auto reply = m_networkAccessManager->get(request);
    connect(reply, &QNetworkReply::finished, this, [ query, this, reply ]() noexcept {
        qDebug() << "============== Events request reply =============";
        qDebug() << "reply.error" << reply->error();
        qDebug() << "reply.isFinished = " << reply->isFinished();
        qDebug() << "reply.url" << reply->url();
        qDebug() << "reply.size:" << reply->size();
        qDebug() << "Content-Type" << reply->header(QNetworkRequest::KnownHeaders::ContentTypeHeader);
        if (reply->isFinished() && reply->error() != QNetworkReply::NetworkError::NoError) {
            qDebug() << "reply failed, removing it...";
            emit this->loadFailed(QPrivateSignal());
            reply->close();
            reply->deleteLater();
            return;
        }
        if (reply->isFinished()) {
            QByteArray buf = reply->readAll();
            QJsonParseError err{};
            QJsonDocument json = QJsonDocument::fromJson(buf, &err);
            if (json.isNull()) {
                qCritical() << "Json parse error:" << err.errorString();
                emit loadFailed(QPrivateSignal());
            }
            if (err.error == QJsonParseError::NoError) {
                m_db->storeCachedEvents(query.toString(), buf);
            }
            m_events = json.object();
            // qDebug() << "result: " << m_events;
            qDebug() << "loadCompleted!";
            emit this->loadCompleted(QPrivateSignal());
            reply->close();
            reply->deleteLater();
        }
    });
    using SignalType = void (QNetworkReply::*)(QNetworkReply::NetworkError);
    connect(reply, static_cast< SignalType >(&QNetworkReply::error),
            this, [this](QNetworkReply::NetworkError error) noexcept {
                qDebug() << "Network error: " << error;
                emit this->loadFailed(QPrivateSignal());
            });
}

const QJsonObject &EventsLoader::events() const
{
    return m_events;
}
