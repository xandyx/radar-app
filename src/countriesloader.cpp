/*
 *   countriesloader.cpp
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "countriesloader.h"
#include "requestutils.h"

#include <QJsonParseError>
#include <QNetworkReply>

CountriesLoader::CountriesLoader(QNetworkAccessManager &networkAccessManager, QObject *parent)
    : QObject(parent)
    , m_networkAccessManager(&networkAccessManager)
    , m_requestUrlBase(QStringLiteral("https://radar.squat.net/api/1.2/search/location.json?fields=%22%22&limit=1"))
{
}

CountriesLoader::~CountriesLoader() = default;

void CountriesLoader::loadCountries()
{
    clear();
    if (m_networkAccessManager->networkAccessible() == QNetworkAccessManager::NetworkAccessibility::UnknownAccessibility) {
        m_networkAccessManager->setNetworkAccessible((QNetworkAccessManager::NetworkAccessibility::Accessible));
    }

    if (m_networkAccessManager->networkAccessible() != QNetworkAccessManager::NetworkAccessibility::Accessible) {
        qCritical() << "Network is not accessible! networkAccessManager->networkAccessible()=" << m_networkAccessManager->networkAccessible(); ;
        emit loadFailed(QPrivateSignal());
        return;
    }
    QNetworkRequest request;
    QUrl requestUrl(m_requestUrlBase, QUrl::ParsingMode::TolerantMode);
    qDebug() << "requestUrl=" << requestUrl.toString();
    configureRequestSSL(request);
    request.setUrl(requestUrl);
    request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("Radar App 1.0"));

    auto reply = m_networkAccessManager->get(request);
    connect(reply, &QNetworkReply::finished, this, [ this, reply ]() noexcept {
        qDebug() << "reply.error" << reply->error();
        qDebug() << "reply.isFinished = " << reply->isFinished();
        qDebug() << "reply.url" << reply->url();
        qDebug() << "reply.size:" << reply->size();
        qDebug() << "Content-Type" << reply->header(QNetworkRequest::KnownHeaders::ContentTypeHeader);
        if (reply->isFinished()) {
            QByteArray buf = reply->readAll();
            QJsonParseError err{};
            QJsonDocument json = QJsonDocument::fromJson(buf, &err);
            if (json.isNull()) {
                qCritical() << "Json parse error:" << err.errorString();
                emit loadFailed(QPrivateSignal());
            }
            m_locations = json.object();
            qDebug() << "loadCompleted!";
            emit this->loadCompleted(QPrivateSignal());
            reply->close();
            reply->deleteLater();
        }
    });
}

const QJsonObject &CountriesLoader::locations() const
{
    return m_locations;
}

void CountriesLoader::clear()
{
    m_locations = {};
}
