/*
 *   database.cpp
 *
 *   Copyright (c) 2022 Andy Ex
 *
 *   This file is part of Radar-App.
 *
 *   Radar-App is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Radar-App is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Radar-App.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "database.h"
#include "locationprovider.h"
#include <tuple>

QVariant DB::insertLocation(const Location &location)
{
    qDebug() << "Storing location wit uuid=" << location.uuid.toString();
    QSqlQuery q;
    q.prepare(QStringLiteral("insert or replace into locations("
                             "uuid, name, country, locality, firstName, "
                             "lastName, postalCode, thoroughfare, "
                             "directions, latitude, longitude"
                             ") values("
                             "?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?"
                             ")"));
    q.addBindValue(location.uuid.toRfc4122());
    q.addBindValue(location.name);
    q.addBindValue(location.country);
    q.addBindValue(location.locality);
    q.addBindValue(location.firstName);
    q.addBindValue(location.lastName);
    q.addBindValue(location.postalCode);
    q.addBindValue(location.thoroughfare);
    q.addBindValue(location.directions);
    if (location.coordinate.isValid()) {
        q.addBindValue(location.coordinate.latitude());
        q.addBindValue(location.coordinate.longitude());
    } else {
        q.addBindValue(QVariant(QVariant::Double));
        q.addBindValue(QVariant(QVariant::Double));
    }
    q.exec();
    return q.lastInsertId();
}

QVariant DB::insertLocations(const QVector< Location > &locations)
{
    if (locations.empty()) {
        return {};
    }
    QSqlQuery q;
    bool transactionIsRunning = QSqlDatabase::database().transaction();
    q.prepare(QStringLiteral("insert or replace into locations("
                             "uuid, name, country, locality, firstName, "
                             "lastName, postalCode, thoroughfare, "
                             "directions, latitude, longitude"
                             ") values("
                             "?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?"
                             ")"));
    for (const auto &location : qAsConst(locations)) {
        q.addBindValue(location.uuid.toRfc4122());
        q.addBindValue(location.name);
        q.addBindValue(location.country);
        q.addBindValue(location.locality);
        q.addBindValue(location.firstName);
        q.addBindValue(location.lastName);
        q.addBindValue(location.postalCode);
        q.addBindValue(location.thoroughfare);
        q.addBindValue(location.directions);
        if (location.coordinate.isValid()) {
            q.addBindValue(location.coordinate.latitude());
            q.addBindValue(location.coordinate.longitude());
        } else {
            q.addBindValue(QVariant(QVariant::Double));
            q.addBindValue(QVariant(QVariant::Double));
        }
        q.exec();
    }
    if (transactionIsRunning) {
        QSqlDatabase::database().commit();
    }
    return q.lastInsertId();
}

std::pair< bool, Location > DB::findLocation(QUuid uuid)
{
    qDebug() << "DB: Loading location by uuid=" << uuid.toString(QUuid::WithoutBraces);
    QSqlQuery q;
    q.prepare(QStringLiteral("select name, country, locality, firstName,"
                             " lastName, postalCode, thoroughfare, directions,"
                             " latitude, longitude from locations where uuid = :uuid"));
    q.bindValue(QStringLiteral(":uuid"), uuid.toRfc4122());
    if (q.exec()) {
        if (q.next()) {
            Location result{};
            result.name = q.value(0).toString();
            result.country = q.value(1).toByteArray();
            result.locality = q.value(2).toString();
            result.firstName = q.value(3).toString();
            result.lastName = q.value(4).toString();
            result.postalCode = q.value(5).toInt();
            result.thoroughfare = q.value(6).toString();
            result.directions = q.value(7).toString();
            if (!q.isNull(8) && !q.isNull(9)) {
                qreal latitude = q.value(8).toDouble();
                qreal longitude = q.value(9).toDouble();
                result.coordinate = QGeoCoordinate(latitude, longitude);
            }
            result.uuid = uuid;
            qDebug() << "found location with name " << result.name;
            return {true, result};
        }
    }
    qDebug() << "Location not found!";
    return {false, {}};
}

QVector< Location > DB::getLocations(const QByteArray &countryCode, const QString &city)
{
    QVector< Location > results;
    qDebug() << "DB: Loading all locations location for " << countryCode << "/" << city;
    QSqlQuery q;
    q.prepare(QStringLiteral("select uuid, name, firstName,"
                             " lastName, postalCode, thoroughfare, directions,"
                             " latitude, longitude from locations where country = :country AND locality = :city"));
    q.bindValue(QStringLiteral(":country"), countryCode);
    q.bindValue(QStringLiteral(":city"), city);
    if (q.exec()) {
        if (q.size() > 0) {
            results.reserve(q.size());
        }
        while (q.next()) {
            Location result{};
            result.uuid = QUuid::fromRfc4122(q.value(0).toByteArray());//
            result.name = q.value(1).toString();
            result.country = countryCode;
            result.locality = city;
            result.firstName = q.value(2).toString();
            result.lastName = q.value(3).toString();
            result.postalCode = q.value(4).toInt();
            result.thoroughfare = q.value(5).toString();
            result.directions = q.value(6).toString();
            if (!q.isNull(7) && !q.isNull(8)) {
                qreal latitude = q.value(7).toDouble();
                qreal longitude = q.value(8).toDouble();
                result.coordinate = QGeoCoordinate(latitude, longitude);
            }
            results.push_back(std::move(result));
        }
    }
    qDebug() << "Loaded " << results.size() << " entries for " << city;
    return results;
}

QSet< QUuid > DB::getAllUUIDs()
{
    QSet< QUuid > retval;
    qDebug() << "Loading available uuids...";
    QSqlQuery q;
    q.prepare(QStringLiteral("select uuid from locations"));

    if (q.exec()) {
        retval.reserve(q.size());
        while (q.next()) {
            retval.insert(QUuid::fromRfc4122(q.value(0).toByteArray()));
        }
    } else {
        qCritical() << "Error selecting location uuids:" << q.lastError().text();
    }
    qDebug() << "Selected " << retval.size() << " UUIDs!";
    return retval;
}

QStringList DB::getAllCountries()
{
    QSqlQuery q;
    q.prepare(QStringLiteral("SELECT name FROM countries"));
    if (q.exec()) {
        QStringList result;
        result.reserve(q.size());
        while (q.next()) {
            result.push_back(q.value(0).toString());
        }
        return result;
    }
    qCritical() << "Failed to SELECT all countries:" << q.lastError();
    return {};
}

QMap< QByteArray, QStringList > DB::getAllCities()
{
    QSqlQuery q;
    q.prepare(QStringLiteral(
        "select code, cities.name from countries, cities where countries.id = cities.countryid ORDER BY code,cities.name"));
    if (q.exec()) {
        QMap< QByteArray, QStringList > result;
        while (q.next()) {
            result[q.value(0).toByteArray()].push_back(q.value(1).toString());
        }
        return result;
    }
    qCritical() << "Failed to SELECT all cities:" << q.lastError();
    return {};
}

QVariant DB::insertCountry(const QByteArray &code, const QString &name, const QStringList &cities)
{
    QSqlQuery q;
    bool transaction = QSqlDatabase::database().transaction();
    q.prepare(QStringLiteral("INSERT OR REPLACE INTO countries(code, name) VALUES(:code, :name)"));
    q.bindValue(QStringLiteral(":code"), code);
    q.bindValue(QStringLiteral(":name"), name);
    q.exec();
    if (!q.lastInsertId().isValid()) {
        qCritical() << "Failed to insert into countries:" << q.lastError().text();
        if (transaction) {
            QSqlDatabase::database().rollback();
        }
        return {};
    }
    int countryId = q.lastInsertId().toInt();

    q.prepare(QStringLiteral("INSERT OR REPLACE INTO cities(countryId, name) VALUES(:countryId, :name)"));
    for (const auto &city : qAsConst(cities)) {
        q.bindValue(QStringLiteral(":countryId"), countryId);
        q.bindValue(QStringLiteral(":name"), city);
        q.exec();
        Q_ASSERT(q.lastInsertId().isValid());
    }
    if (transaction) {
        bool commited = QSqlDatabase::database().commit();
        qDebug() << "insertCountry() transaction commited=" << commited;
    }
    return q.lastInsertId();
}

QVariant DB::getCachedEvents(const QString &query)
{
    QVariant result;
    QSqlQuery q;
    q.prepare(QStringLiteral("select timestamp, json FROM events_cache WHERE query = :query ORDER BY timestamp DESC LIMIT 1"));
    q.bindValue(QStringLiteral(":query"), query);
    if (!q.exec()) {
        qCritical() << "Can not select by query:" << q.lastError().text();
        return result;
    }
    qInfo() << "DB::getCachedEvents result size:" << q.size();
    if (q.lastError().isValid()) {
        qCritical() << "Can not select json:" << query << "error:" << q.lastError().text();
        Q_ASSERT(false); //FIXME
    } else if (q.next()) {
        result.setValue(q.value(1).toByteArray());
    }
    return result;
}

void DB::storeCachedEvents(const QString &query, const QByteArray &json)
{
    QSqlQuery q;
    q.prepare(QStringLiteral("insert OR replace INTO events_cache(query, timestamp, json) VALUES(:query, :timestamp, :json)"));
    q.bindValue(QStringLiteral(":query"), query);
    q.bindValue(QStringLiteral(":timestamp"), QDateTime::currentSecsSinceEpoch());
    q.bindValue(QStringLiteral(":json"), json);
    q.exec();

    Q_ASSERT(q.lastInsertId().isValid()); //FIXME
    if (!q.lastInsertId().isValid()) {
        qCritical() << "Can not cache query" << query << " :" << q.lastError().text();
    }
}

void DB::clearCachedEvents(const QString &query)
{
    QSqlQuery q;
    q.prepare(QStringLiteral("delete FROM events_cache WHERE query=%1").arg(query));
    if (!q.exec()) {
        qCritical() << "Can not delete cache for query:" << query << "error:" << q.lastError().text();
        Q_ASSERT(false); //FIXME
    }
}

void DB::clearCountries()
{
    qDebug() << __PRETTY_FUNCTION__;
    QSqlQuery q;
    q.prepare(QStringLiteral("delete from countries"));
    if (q.exec()) {
        qDebug() << "success";
    }
    qCritical() << "Error deleting countries:" << q.lastError().text();
}

void DB::clearCities()
{
    qDebug() << __PRETTY_FUNCTION__;
    QSqlQuery q;
    q.prepare(QStringLiteral("delete from cities"));
    if (q.exec()) {
        qDebug() << "success";
    }
    qCritical() << "Error deleting cities:" << q.lastError().text();
}

DB::DB(QObject *parent)
    : QObject(parent)
    , m_db(QSqlDatabase::addDatabase(QStringLiteral("QSQLITE")))
{
    Q_ASSERT(m_db.isValid());
}

DB::~DB() = default;

namespace Queries
{
    QString addLocationsTable()
    {
        return QStringLiteral("create table locations("
                              " uuid varchar primary key, name varchar,"
                              " country varchar,"
                              " locality varchar, firstName varchar,"
                              " lastName varchar, postalCode integer,"
                              " thoroughfare varchar, directions varchar,"
                              " latitude varchar, longitude varchar)");
    }

    QString addCountriesTable()
    {
        return QStringLiteral("CREATE TABLE countries(id integer primary key,"
                              " code varchar, name varchar)");
    }
    QString addCitiesTable()
    {
        return QStringLiteral("CREATE TABLE cities(id INTEGER PRIMARY KEY,"
                              "    countryId VARCHAR,"
                              "    name varchar,"
                              "    FOREIGN KEY(countryId) REFERENCES countries(code)"
                              ")");
    }

    QString addEventsCacheTable()
    {
        return QStringLiteral("CREATE TABLE events_cache(query VARCHAR PRIMARY KEY, timestamp INTEGER, json VARCHAR)");
    }

}// namespace Queries

QSqlError DB::initDB()
{
    QFile asset(QStringLiteral(":/data/db.sqlite"));
    if (!asset.exists()) {
        qCritical() << "No DB file in assets!";
        return {};
    }
    const auto dir = QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    if (!dir.mkpath(dir.absolutePath())) {
        qCritical() << "Can not create dir " << dir.absolutePath();
        return {};
    }
    const auto targetPath = dir.filePath(QStringLiteral("radar-app.db"));
    qDebug() << "DB file path: " << targetPath;
    if (!QFile::exists(targetPath)) {
        qDebug() << "copying DB from assets...";
        if (!dir.exists(targetPath) && !asset.copy(targetPath)) {
            qCritical() << "Can not copy to " << targetPath;
            return {};
        }
        QFile(targetPath).setPermissions(QFileDevice::Permission::ReadOwner | QFileDevice::Permission::WriteOwner);
    }
    m_db.setDatabaseName(targetPath);

    if (!m_db.open()) {
        return m_db.lastError();
    }

    QSqlQuery q;
    QStringList tables = m_db.tables();

    const std::initializer_list< std::pair< QString, QString > > tableQueryPairs = {
        {QStringLiteral("locations"), Queries::addLocationsTable()},
        {QStringLiteral("countries"), Queries::addCountriesTable()},
        {QStringLiteral("cities"), Queries::addCitiesTable()},
        {QStringLiteral("events_cache"), Queries::addEventsCacheTable()},
    };

    for (const std::pair< QString, QString > &tableQueryPair : tableQueryPairs) {
        if (tables.contains(tableQueryPair.first, Qt::CaseInsensitive)) {
            qInfo() << "DB already contains the table " << tableQueryPair.first;
            continue;
        }
        if (!q.exec(tableQueryPair.second)) {
            return q.lastError();
        }
    }

    return {};
}
